<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:api')->get('/user', function (Request $request) {return $request->user();});
Route::post('register', 'API\UserController@register');
Route::post('login', 'API\UserController@login');
Route::post('perfiles', 'API\UserController@perfiles');




Route::group(['middleware' => 'auth:api'], function(){
    Route::post('details', 'API\UserController@details');
    Route::post('logout', 'API\UserController@logout');
    Route::post('usuario_perfil', 'API\UserController@user_perfiles');      
    Route::post('usuario_perfil_detalle', 'API\UserController@usuario_perfil_detalle');   
    Route::post('admin_user', 'API\AdminController@Admin_users');    
    Route::post('admin_user_detalle', 'API\AdminController@Admin_users_detalle');
    Route::post('admin_user_update_password', 'API\AdminController@Update_password');  
    Route::post('actualizar_datos_usuario', 'API\AdminController@Actualizar_datos_usuario');
    Route::post('devuelve_ambulatorio', 'API\DevuelveTodoPaciente\AmbulatorioController@DevuelveInfoAmbulatorio');
    Route::post('devuelve_urgencia', 'API\DevuelveTodoPaciente\UrgenciaController@DevuelveInfoUrgencia');
    Route::post('devuelve_quirurgico', 'API\DevuelveTodoPaciente\QuirurgicoController@DevuelveInfoQuirurgico');
    Route::post('devuelve_hospitalizado', 'API\DevuelveTodoPaciente\HospitalizadoController@DevuelveInfoHospitalizado');
    Route::post('devuelve_paciente_sistema', 'API\DevuelveTodoPaciente\PacienteSistemaModuloController@DevuelvePacienteSistema');
    Route::post('devuelve_datos_paciente', 'API\DevuelveTodoPaciente\MaestroPacienteController@DevuelveDatosPacientes');
    Route::post('devuelve_datos_modulo_paciente', 'API\DevuelveTodoPaciente\ModuloPacienteController@DevuelveModuloPaciente');
    Route::post('devuelve_cantidad_atencion', 'API\UrgenciaFlorence\CantidadAtencionesController@CantidadAtenciones');

});    





    


     
