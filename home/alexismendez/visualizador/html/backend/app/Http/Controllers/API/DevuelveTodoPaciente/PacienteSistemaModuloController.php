<?php

namespace App\Http\Controllers\API\DevuelveTodoPaciente;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\paciente_sistema;
use App\maestro_paciente;

class PacienteSistemaModuloController extends Controller
{
    //
    public $successStatus = 200;


    public function DevuelvePacienteSistema(Request $request){
    //public function DevuelvePacienteSistema(){


        $input = $request->all(); 
        $pac_rut = $input['id'];


        /*$paciente_sistema = paciente_sistema::select('paciente_sistemas.sistema_id', 'sistemas.sistema_descripcion') 
                ->join('maestro_pacientes', 'maestro_pacientes.maestro_paciente_id', '=', 'paciente_sistemas.maestro_paciente_id')
                ->join('sistemas', 'sistemas.sistema_id', '=', 'paciente_sistemas.sistema_id')
			    ->where('maestro_pacientes.maestro_paciente_rut', $pac_rut)
                ->distinct()
                ->get();
        */
                
        $paciente_sistema = paciente_sistema::select('paciente_sistema_modulos.sistema_id', 'sistemas.sistema_descripcion') 
        ->join('maestro_pacientes', 'maestro_pacientes.maestro_paciente_id', '=', 'paciente_sistema_modulos.maestro_paciente_id')
        ->join('sistemas', 'sistemas.sistema_id', '=', 'paciente_sistema_modulos.sistema_id')
		->where('maestro_pacientes.maestro_paciente_rut', $pac_rut)
        ->distinct()
        ->get();

                //$paciente_sistema->toSql();

                if ($paciente_sistema->isEmpty()) {
                
                    $paciente_sistema = 'null'; 
        
                    return $paciente_sistema; 
        
                }else{

                    return response()->json($paciente_sistema, $this-> successStatus); 
                }

    } 


}

