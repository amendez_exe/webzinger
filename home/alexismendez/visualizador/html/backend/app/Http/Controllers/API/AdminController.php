<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use App\Perfil; 
use App\User_perfil;
use Illuminate\Support\Facades\Auth; 
use Validator;
  

class AdminController extends Controller 
{
public $successStatus = 200;

    /** 
     * User api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function Admin_users(Request $request) 
    { 
        $user = User::all(); 
        return response()->json([$user], $this-> successStatus); 
    } 
    
    public function Admin_users_detalle(Request $request) 
    { 
        $input = $request->all(); 
        $id = $input['id'];

        $user = User_perfil::select('perfil_id', 'name', 'email')
                            ->where('user_id', $id)
                            ->join('users', 'users.id', '=', 'user_perfil.user_id')
                            ->first();
        
        //$user = Crypt::decrypt($user['password']);

       
        return response()->json([$user], $this-> successStatus); 
    }


    public function Update_password(Request $request){

        $input = $request->all(); 
        $password = $input['password'];  
        $id = $input['id'];  
        $password_ = bcrypt($password); 

        $user = User::where('id', $id )
                      -> update ( [ 'password' => $password_]);


        return response()->json([$user], $this-> successStatus); 
    }


    public function Actualizar_datos_usuario(Request $request){

        $datos          =   $request->all(); 
        $nombrecompleto =   $datos['nombrecompleto'];
        $rut            =   $datos['rut'];
        $perfil         =   $datos['perfil'];
        $id             =   $datos['id'];
        $save           =   true;

        $user = User::select('id')
                      -> where('email', $rut)
                      -> first(); 

        if(trim($user['id']) === '' ){ 
            
            $res = "Se puede guardar"; 
            

        } else {

            if($user['id'] != $id){
                $res =  "El rut ya existe en el sistema, no se puede guardar"; 
                $save = false;
                
            }else { 
                $res = "Se puede guardar"; 
    
            }           
        }               
    
        
    
        if($save === true){
            
            $user = User::where('id', $id )
                      -> update ( [ 'email' => $rut,  'name' => $nombrecompleto]
                                  
                                 );

            $perfil =  User_perfil::where('user_id', $id)
                                -> update ( [ 'perfil_id' => $perfil ] );

            $res = "Datos actualizados";
            
            
        }   

        return $res;      
        
        //return response()->json([$user]);

                      

    }


}