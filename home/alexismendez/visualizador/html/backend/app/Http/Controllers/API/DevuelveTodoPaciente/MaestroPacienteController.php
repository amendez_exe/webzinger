<?php

namespace App\Http\Controllers\API\DevuelveTodoPaciente;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\maestro_paciente;
use DB;

class MaestroPacienteController extends Controller
{
    public $successStatus = 200;

    public function DevuelveDatosPacientes(Request $request){
    
    
            $input = $request->all(); 
            $pac_rut = $input['id'];
            //$datos_paciente = maestro_pacientes::all();
            
            $datos_paciente = DB::connection('sqlsrv')->table('maestro_paciente')->where('maestro_paciente_rut',"=",$pac_rut)
            ->get();
            //->toSql();

            if ($datos_paciente->isEmpty()) {
                
                $datos_paciente = 'null'; 
    
                return $datos_paciente; 
    
            }else{
            
            return response()->json($datos_paciente, $this-> successStatus); 
    
            }
        } 
}
