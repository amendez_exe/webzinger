<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use App\Perfil; 
use App\User_perfil;
use Illuminate\Support\Facades\Auth; 
use Validator;


class UserController extends Controller 
{
public $successStatus = 200;
/** 
     * login api  
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function login(Request $request){ 
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('Password Token')-> accessToken; 
            $success['name'] = $user->name;
            $success['email'] = $user->email;
            $success['id'] = $user->id;
            return response()->json(['success' => $success], $this-> successStatus); 
        } 
        else{ 
            return response()->json(['error'=>'Unauthorised'], 401); 
        } 
    }

    /** 
     * logout api 
     * 
     * @return \Illuminate\Http\Response 
     */
    
     public function logout(){
        $value = $request->bearerToken();
        $id = (new Parser())->parse($value)->getHeader('jti');
        $token = $request->user()->tokens->find($id);
        $token->revoke();

        $response = 'You have been logged out';
        return response()->json(['success' => response], 200);
     }
    /** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response   
     */ 
    public function register(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            //'email' => 'required|email', 
            'email' => 'required', 
            'password' => 'required', 
            'c_password' => 'required|same:password', 
        ]);
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        $input = $request->all(); 
        $input['password'] = bcrypt($input['password']); 
        $user = User::create($input); 
        $success['token'] =  $user->createToken('MyApp')-> accessToken; 
        $success['name'] =  $user->name;
        $user_id =  $user->id;


        $user_perfil = new User_perfil;
        $user_perfil -> user_id =  $user_id; 
        $user_perfil -> perfil_id =  $input['perfil'];
        $user_perfil -> save(); 


        return response()->json(['success'=>$success], $this-> successStatus); 
    }
/** 
     * details api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function details(Request $request) 
    { 
        $user = Auth::user(); 
        return response()->json(['success' => $user], $this-> successStatus); 
    } 

    public function perfiles(Request $request)
    {
        $perfiles = Perfil::all();
        return response()->json( ['success' => $perfiles], $this-> successStatus); 
    }

    public function user_perfiles(Request $request)
    {
        $perfiles = User_perfil::all();
        return response()->json([$perfiles], $this-> successStatus); 
    }

    public function usuario_perfil_detalle(Request $request)
    {
        
        $input = $request->all(); 
        $id = $input['id'];

        $perfil_detalle = User_perfil::where('user_id', $id)->first();
        return response()->json([$perfil_detalle], $this-> successStatus); 
    }
}
