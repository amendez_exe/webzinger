<?php

namespace App\Http\Controllers\API\DevuelveTodoPaciente;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\modulo;
use App\paciente_sistema;
use App\maestro_paciente;

use Illuminate\Support\Facades\DB;


class ModuloPacienteController extends Controller
{
    
    public $successStatus = 200;


    public function DevuelveModuloPaciente(Request $request){


        $input = $request->all(); 
        //$pac_rut = $input['rut'];
        $id = $input['idPac'];
        $idSistema = $input['idSistema'];
        
        /* Modulos habilitados x sistema y paciente */
        /*
        $modulo_paciente=DB::table('modulos')       
            ->selectRaw("modulos.modulo_id,modulos.modulo_descripcion,
            (case when paciente_sistemas.sistema_id::text != '' then '' else 'disabled' end) as accion,(case when paciente_sistemas.sistema_id::text != '' then 'enabled' else 'disabled' end) as accion2") 
            ->leftjoin('paciente_sistemas', 'paciente_sistemas.modulo_id',
            '=',
            DB::raw("modulos.modulo_id
            AND paciente_sistemas.sistema_id ='".$idSistema."' 
            AND maestro_paciente_id ='". $id ."'
            "))
            ->orderBy('modulo_id')
            ->distinct()
            ->get();
        */
        
            $modulo_paciente=DB::table('modulos')       
            ->selectRaw("modulos.modulo_id,modulos.modulo_descripcion,(case when paciente_sistema_modulos.sistema_id::text != '' then '' else 'disabled' end) as accion,(case when paciente_sistema_modulos.sistema_id::text != '' then 'enabled' else 'disabled' end) as accion2") 
            ->leftjoin('paciente_sistema_modulos', 'paciente_sistema_modulos.modulo_id','=',DB::raw("modulos.modulo_id AND paciente_sistema_modulos.sistema_id ='".$idSistema."' AND maestro_paciente_id ='". $id ."'"))
            ->orderBy('modulo_id')
            ->distinct()
            ->get();


    

        if ($modulo_paciente->isEmpty()) {
                
            $modulo_paciente = 'null'; 

            return $modulo_paciente; 

        }else{

            //return response()->json($modulo_paciente, $this-> successStatus); 
            return response()->json(['modulos' =>$modulo_paciente], $this-> successStatus);

        }
        

    } 




}
