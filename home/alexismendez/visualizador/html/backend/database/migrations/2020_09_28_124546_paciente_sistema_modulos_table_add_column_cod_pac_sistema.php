<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PacienteSistemaModulosTableAddColumnCodPacSistema extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('paciente_sistema_modulos', function (Blueprint $table) {

            $table->string('codigo_paciente',25)->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('paciente_sistema_modulos', function (Blueprint $table) {
            $table->dropColumn('codigo_paciente');
        });
    }
}
