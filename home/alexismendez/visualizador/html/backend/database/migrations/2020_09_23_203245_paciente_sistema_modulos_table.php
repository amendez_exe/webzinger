<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PacienteSistemaModulosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paciente_sistema_modulos', function (Blueprint $table) {
            $table->bigIncrements('paciente_sistema_mod_id')->nullable(false);
            $table->integer('maestro_paciente_id')->nullable();
            $table->integer('sistema_id')->nullable();
            $table->integer('modulo_id')->nullable();

            $table->foreign('maestro_paciente_id')->references('maestro_paciente_id')->on('maestro_pacientes');
            $table->foreign('sistema_id')->references('sistema_id')->on('sistemas');
            $table->foreign('modulo_id')->references('modulo_id')->on('modulos');

            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paciente_sistema_modulos');
    }
}
