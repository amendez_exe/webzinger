import { User } from './../interfaces/user';
import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {UserService} from '../services/user.service';

@Component({
  selector: 'app-conversation',
  templateUrl: './conversation.component.html',
  styleUrls: ['./conversation.component.scss']
})

export class ConversationComponent implements OnInit {
  friendId: any;
  friend: any;
  
  constructor(private activatedRoute: ActivatedRoute,
              private userService: UserService) {
    this.friendId = this.activatedRoute.snapshot.params['uid'];
    console.log(this.friendId);
 

   this.userService.getUserById(this.friendId).valueChanges().subscribe((item) =>{

    this.friend = item;
  }, (err) => {

   console.log(err)   
  });
    
 

  }

  ngOnInit() {
  }

}