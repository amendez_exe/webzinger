import { UserService } from './../services/user.service';
import { User } from './../interfaces/user';
import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

 friends: any;
 query:string = '';
 user: any;
 

  constructor(private userService: UserService,private authenticationService: AuthenticationService) {

    this.authenticationService.getStatus().subscribe((status) => {
      this.userService.getUserById(status.uid).valueChanges().subscribe((data) => {
        this.user = data;
        console.log(this.user);
      }, (error) => {
        console.log(error);
      });
    }, (error) => {
      console.log(error);
    });
 

   this.userService.getUsers().valueChanges().subscribe((item) =>{

    this.friends = item;
  }, (err) => {

   console.log(err)   
  });

    }

    

  ngOnInit(): void {
  }

}
