import { AngularFireStorageModule } from '@angular/fire/storage';
import { Component, OnInit } from '@angular/core';
import { ImageCroppedEvent, LoadedImage } from 'ngx-image-cropper';
import { AuthenticationService } from '../services/authentication.service';
import { UserService } from '../services/user.service';
import { AngularFireStorage } from '@angular/fire/storage';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  user: any;
  imageChangedEvent: any = '';
  croppedImage: any = '';
  picture: any;

  constructor(private userService: UserService, private authenticationService: AuthenticationService, private firebaseStorage: AngularFireStorage) {
    this.authenticationService.getStatus().subscribe((status) => {
      this.userService.getUserById(status.uid).valueChanges().subscribe((data) => {
        this.user = data;
        console.log(this.user);
      }, (error) => {
        console.log(error);
      });
    }, (error) => {
      console.log(error);
    });
  }

  ngOnInit() {
  }
  saveSettings() {
    if(this.croppedImage){
     const currentPictureId = Date.now();
     const pictures = this.firebaseStorage.ref('pictures/' + currentPictureId + '.jpg').putString(this.croppedImage, 'data_url');
     pictures.then((result:any) => {

      this.picture = this.firebaseStorage.ref('pictures/' + currentPictureId + '.jpg').getDownloadURL();
      this.picture.subscribe((p: any) => {
       
        this.userService.setAvatar(p, this.user.uid).then(() =>{

          alert('Avatar subido correctamente');
        }).catch((error) =>{
          alert('Error en la subida del avatar');
          console.log(error);
        });

      })

     }).catch((error: any) =>{

      console.log(error);
     })
    }
    else{
    this.userService.editUser(this.user).then(() => {
      alert('Cambios guardados!');
    }).catch((error) => {
      alert('Hubo un error');
      console.log(error);
    });
  }
  }

fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
}
imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
}
imageLoaded(image: LoadedImage) {
    // show cropper
}
cropperReady() {
    // cropper ready
}
loadImageFailed() {
    // show message
}

}
