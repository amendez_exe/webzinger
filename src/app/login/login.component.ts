import { UserService } from './../services/user.service';
import { AuthenticationService } from './../services/authentication.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../interfaces/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

 operation:string = 'login';
 email: string = '';
 password: string = '';
 nick:string = '';

  constructor(private authenticationService: AuthenticationService, private route: Router, private userService: UserService) { }

  ngOnInit(): void {
  }

  login(){

    this.authenticationService.loginWithEmail(this.email, this.password).then((data) =>{

      this.route.navigate(['home']);
      console.log(data);

    }).catch((error) => {
      alert('error');
      console.log(error);

    });
  }

  register(){

    this.authenticationService.createWithEmail(this.email, this.password).then((data) =>{

const user:User = {
uid: data.user.uid,
email: this.email,
nick: this.nick,
friend: true,
state: 'online'
}

this.userService.createUser(user).then((item) =>{

  alert('Registrado');
  console.log(item);

}).catch((err) =>{
console.log(err);
});

    
    }).catch((error) => {
      alert('error');
      console.log(error);

    });
  }

}
