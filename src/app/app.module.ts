import { AuthenticationGuard } from './services/authentication.guard';
import { PipeSearch } from './pipes/search';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { ConversationComponent } from './conversation/conversation.component';
import { ProfileComponent } from './profile/profile.component';
import { MenuComponent } from './menu/menu.component';
import { FormsModule } from '@angular/forms';
import {environment} from '../environments/environment';
import {AngularFireModule} from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { ImageCropperModule } from 'ngx-image-cropper';



const appRoutes: Routes = [
{path: "", component: HomeComponent, canActivate: [AuthenticationGuard]},
{path:"home", component: HomeComponent, canActivate: [AuthenticationGuard]},
{path:"login", component: LoginComponent},
{path:"conversation/:uid", component: ConversationComponent, canActivate: [AuthenticationGuard]},
{path:"profile", component: ProfileComponent, canActivate: [AuthenticationGuard]}

];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    ConversationComponent,
    ProfileComponent,
    MenuComponent,
    PipeSearch
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,  
    AngularFireAuthModule,  
    AngularFireStorageModule,
    AngularFireDatabaseModule,
    ImageCropperModule
  
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
