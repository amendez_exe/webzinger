import { User } from './../interfaces/user';
import { Injectable } from '@angular/core';
import {AngularFireDatabase} from 'angularfire2/database';

@Injectable({
  providedIn: 'root'
})
export class UserService {

 user!: User;

  constructor(private angularDatabase: AngularFireDatabase) { 

  //  let usuario1: User = {nick:'waldo', subnick:'peron', age: 34, email: 'waldo@gmail.com', friend: true, uid: 1234, state: 'offline'};
  //  let usuario2: User = {nick:'Reina', subnick:'amigdala', age:44, email:'reina@gmail.com', friend:true, uid:1235, state: 'busy'};
  //  let usuario3: User = {nick:'Jorge', subnick:'feo', age:47, email:'feo@gmail.com', friend:false, uid:1236, state: 'online'};
  //  let usuario4: User = {nick:'Richard', subnick:'richard', age:54, email:'richard.rfcn@gmail.com', friend:true, uid:1237, state: 'online'};
  //  let usuario5: User = {nick:'Claudio', subnick:'waton', age:56, email:'waton@gmail.com', friend:true, uid:1238, state: 'busy'};
  //  let usuario6: User = {nick:'Alexis', subnick:'alex', age:44, email:'heralexis2000@gmail.com', friend:true, uid:1239, state: 'online'};

  //  this.friends = [usuario1, usuario2, usuario3, usuario4, usuario5, usuario6];

  }

 
getUsers(){

 return this.angularDatabase.list('/users');
}

getUserById(uid: any){

  return this.angularDatabase.object('/users/' + uid);
 }

 createUser(user:User){

  return this.angularDatabase.object('/users/' + user.uid ).set(user);
 }

 editUser(user:User){

  return this.angularDatabase.object('/users/' + user.uid ).set(user);
 }
 setAvatar(avatar: any, uid: any) {
  return this.angularDatabase.object('/users/' + uid + '/avatar').set(avatar);
}


}
