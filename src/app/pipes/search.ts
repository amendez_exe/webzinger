import { User } from './../interfaces/user';
import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
name: 'search'
})
export class PipeSearch  implements PipeTransform {

  public transform(value: User[], args:string):User[] {
      
if(!value){
        return [];
    }
if(!args){
    return value;
    }

    args = args.toLowerCase();

    return value.filter((item) => {

      return JSON.stringify(item).toLowerCase().includes(args);
    })

    }
}